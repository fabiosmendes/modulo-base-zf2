<?php

namespace Base\Fixture;

use PHPUnit_Framework_TestCase;

use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class TestDataFixture extends PHPUnit_Framework_TestCase
{
    /**
     * Service Manager
     * @var Zend\ServiceManager\ServiceManager
     */
    protected $sm;

    /**
     * Doctrine Entity Manager
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;
    
    /**
     *
     * @var Doctrine\ORM\Tools\SchemaTool 
     */
    protected $schemaTool;
    
    /**
     *
     * @var Doctrine\ORM\Mapping\ClassMetadata 
     */
    protected $classes;
    
    
    public function setUp()
    {
        if(null === $this->sm) {
            return;
        }
        
        $this->em = $this->sm->get('Doctrine\ORM\EntityManager');
        
        parent::setUp();
        $this->schemaTool = new \Doctrine\ORM\Tools\SchemaTool($this->em);
        $this->classes = $this->em->getMetadataFactory()->getAllMetadata();
        
        $this->schemaTool->dropSChema($this->classes);
        $this->schemaTool->createSchema($this->classes);
    }
    
    public function loadFixtures(array $loadFixtures)
    {
        $loader = new Loader();
        foreach($loadFixtures as $loadFixture) {
            $loader->addFixture($loadFixture);
        }
        
        $fixtures = $loader->getFixtures();
        
        $executor = new ORMExecutor($this->em, new ORMPurger());
        $executor->execute($fixtures, false);
    }
    
    public function tearDown()
    {
        if ( class_exists('SchemaTool') && method_exists($this->schemaTool, 'dropSchema') ) {
            $this->schemaTool->dropSchema($this->classes);
        }

        if ( class_exists('SchemaTool') && method_exists($this->schemaTool, 'dropDatabase')) {
            $this->schemaTool->dropDatabase();
        }

        unset($this->sm);
        unset($this->em);
        unset($this->schemaTool);
        unset($this->classes);
        
        parent::tearDown();
    }
}
