<?php

namespace Base\Log;

/**
 * Use in Module.php 
 * Ex: 
 *      new LogError();
 */
class LogError
{
    public function __construct()
    {
        $old_error_handler = set_error_handler(array($this, 'register'));
    }
    
    
    public function register($num, $err, $file, $line)
    {
        $data = new \DateTime("now");
        $str = "";
        $str .= $data->format('H:i:s') . "\n";
        $str .= "\tNUM. ERRO: " . $num . "\n";
        $str .= "\tERRO: " . wordwrap($err, 160)  . "\n";
        $str .= "\tARQUIVO: " . $file . "\n";
        $str .= "\tLINHA: " . $line . "\n\n\n";
        
        echo "<!-- Error Log. -->\n";
        echo $str."\n\n";
        
        $this->gravarErro($str);
        //LogError::enviaEmail($str);

        return true;
    }

    public function gravarErro($registro)
    {
        $dateTime = new \DateTime("now");
        $data = 'log-error-'.$dateTime->format('d-m-Y');
        $arquivo = getcwd() . '/data/log/' . $data . '.txt';
        $f = fopen($arquivo, 'a+');
        
        fwrite($f, $registro);
        fclose($f);
    }
    
    public function enviaEmail($mensagem)
    {
        $emailDest = 'contato@redsuns.com.br';
        $return_path = $emailDest;
        $from = $emailDest;

        $assunto = 'Erro Site - ' . SITE_NAME;

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "Return-Path: " . $return_path . "\r\n";
        $headers .= "From: " . $from . "\r\n";

        mail($emailDest, $assunto, $mensagem, $headers);
    }

}

