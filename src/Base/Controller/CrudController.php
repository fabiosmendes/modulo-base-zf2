<?php

namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;
use Base\Util\StringUtil;
use Base\Util\Paginator as Paginacao;

class CrudController extends AbstractActionController
{
    /**
     * @var EntityManager 
     */
    protected $em;
    protected $authService;
    protected $service;
    protected $entity;
    protected $form;
    protected $route;
    protected $controller;

    public function __construct()
    {
        $this->layout()->controllerName = array('control' => $this->controller);
    }
    
    public function getAuthService()
    {
        return $this->authService;
    }

    public function getIdentity($namespace = null)
    {
        $sessionStorage = new SessionStorage($namespace);
        $this->authService = new AuthenticationService();
        $this->authService->setStorage($sessionStorage);

        if($this->getAuthService()->hasIdentity()) {
            return $this->getAuthService()->getIdentity();
        }
    }
    
    /**
     * 	
     * @param  string $controllerName
     * @param  array  $pars 
     * @return void
     * @access public
     * @date   05/07/2010 - 12:32:16
     */
    public function preProcessadorBusca($controllerName, $pars)
    {
        $i = 0;
        foreach($pars as $valor) {
            if($valor != '')
                ++$i;
        }

        $buscaId = '';
        if($i > 0) {
            $buscaId = serialize($pars);
        }   

        $pagerPars = $session = new Container('busca_' . $controllerName);
        if(!isset($pagerPars->parsBusca) || ($buscaId != '' && $pagerPars->parsBusca != $buscaId)) {
            $pagerPars->parsBusca = $buscaId;
            $parametros = unserialize($pagerPars->parsBusca);
        } else {
            $par = StringUtil::getLastItemUrl($this->getRequest()->getRequestUri());

            if($par == 'busca') {
                unset($pagerPars->parsBusca);
                $parametros = array();
            } else {
                $parametros = unserialize($pagerPars->parsBusca);
            }
        }

        return $parametros;
    }

    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   04/07/2010 - 15:23:00
     */
    public function parametersPager($limit = 10)
    {
        /**
         * Parametros paginação
         */
        $offset = $this->params()->fromRoute('pagina', 0);
        $offset = sprintf('%d', empty($offset) ? '0' : $offset);

        if($offset > 1) {
            if($offset == 2) {
                $offset = $limit;
            } else {
                $offset = $limit * ($offset - 1);
            }    
        }

        $pars['limit'] = $limit;
        $pars['offset'] = $offset;
        $pars['itensPager'] = 2;

        return $pars;
    }

    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   04/07/2010 - 14:35:22
     */
    public function pager($linkPagina, $total, $itensPerPage = 10, $itensPagination = 15)
    {
        $page = StringUtil::getItemNaUrl('pagina');
        $page = sprintf('%d', empty($page) ? 1 : $page);

        $paginator = new Paginacao();

        $paginator->setTotal($total);
        $paginator->setItensPerPage($itensPerPage);
        $paginator->setCurrentPage($page);
        $paginator->setNumItensPagination($itensPagination);

        $paginator->createStructure();

        return array('linkPagina' => $linkPagina, 'paginator' => $paginator);
    }

    protected function getParsURL($explodePar = "/busca/")
    {
        $server = $this->getRequest()->getServer();
        $requestURI = $server['REQUEST_URI'];
        $parts = explode($explodePar, $requestURI);

        $parsURI = array();
        if(isset($parts[1])) {
            $parsURI = explode("/", $parts[1]);
        }    
        $pars = array();
        $numItens = count($parsURI);
        for($i = 0; $i < $numItens; ++$i) {
            if(isset($parsURI[$i + 1])) {
                $pars[$parsURI[$i]] = $parsURI[$i + 1];
                ++$i;
            }
        }

        return $pars;
    }

    public function indexAction() 
    {   
        $service  = $this->getServiceLocator()->get($this->service);
        $searchPars = $this->params()->fromQuery();
        $list     = $service->getList($searchPars);
        
        $page = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);
        
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }

    public function viewAction()
    {
        $repository = $this->getEm()->getRepository($this->entity);
        $id = $this->params()->fromRoute('id', 0);
        $entity = $repository->find($id);

        return $this->renderView(array('entity' => $entity));
    }

    public function newAction()
    {
        $form = new $this->form();
        $request = $this->getRequest();

        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $service->insert($postData);
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }

        return $this->renderView(array('form' => $form));
    }

    public function editAction()
    {
        $form = new $this->form();

        $request = $this->getRequest();

        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $service->update($postData);

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        } else {
            $repository = $this->getEm()->getRepository($this->entity);
            $id = $this->params()->fromRoute('id', 0);
            $entity = $repository->find($id);

            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            } else {
                $form->setData($entity->toArray());
            }
        }

        return $this->renderView(array('form' => $form));
    }

    public function deleteAction()
    {
        $service = $this->getServiceLocator()->get($this->service);
        if($service->delete($this->params()->fromRoute('id', 0))) {
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
        }
    }

    public function renderView(array $pars)
    {
        // Seta nome do controller para marcação do Menu
        $this->layout()->controllerName = $this->controller;

        $view = new ViewModel();
        foreach($pars as $nome => $valor) {
            $view->setVariable($nome, $valor);
        }

        return $view;
    }

    /**
     * @return EntityManager     
     */
    protected function getEm()
    {
        if(null == $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->em;
    }
}
