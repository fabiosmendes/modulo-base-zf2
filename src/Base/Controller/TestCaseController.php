<?php

namespace Base\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class TestCaseController extends AbstractHttpControllerTestCase 
{
    /**
     * Service Manager
     * @var Zend\ServiceManager\ServiceManager
     */
    protected $sm;

    /**
     * Doctrine Entity Manager
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;
    
    
    public function setUp()
    {
        $this->traceError = true;
        
        $this->sm = $this->getApplicationServiceLocator();
        $this->em = $this->sm->get('Doctrine\ORM\EntityManager');
        
        parent::setUp();
    }
    
    
    public function tearDown()
    {
        unset($this->sm);
        unset($this->em);

        parent::tearDown();
    }
}