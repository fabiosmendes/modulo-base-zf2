<?php

namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class BaseController extends AbstractActionController 
{    
    /**
     * @var EntityManager 
     */
    protected $em;
    
    /**
     *
     * @var \Zend\View\Model\ViewModel
     */
    protected $viewModel;
    
    /**
     *
     * @var array 
     */
    protected $postData = array();
    
    /**
     *
     * @var array 
     */
    protected $parsView = array();
    
    public function __construct()
    {
        $event = $this->getEvent();
        $this->viewModel = $event->getViewModel(); // viewModel global
    }
    
    public function checkPostData()
    {
        $request = $this->getRequest();
        $isPost  = $request->isPost();
        if($isPost) {
            $this->postData = $request->getPost()->toArray();
        }  
        
        return $isPost;
    }
    
    public function formIsValid($form) 
    {
        $form->setData($this->getPostData());
        return $form->isValid();
    }
    
    public function getPostData()
    {
        return $this->postData;
    }
    
    public function renderView(array $pars) // Global 
    {
        $this->viewModel->setVariables($pars);
        return $this->viewModel;
    }
    
    /**
     * Utiliza a ViewModel para exibir os parâmetros
     * 
     * @param array $pars
     * @return \Zend\View\Model\ViewModel
     */
    public function renderViewLocal(array $pars) 
    {
        $view = new ViewModel();
        $view->setVariables($pars);
        //$result->setTerminal(true);
        //$result->setTemplate('front/email/contato-site.phtml');
        return $view;
    }
    
    /**
     * Utiliza o JsonModel para exibir os parâmetros
     * 
     * @param array $pars
     * @return \Zend\View\Model\JsonModel
     */
    public function renderJson(array $pars) 
    {
        $jsonModel = new JsonModel();
        $jsonModel->setVariables($pars);
        
        return $jsonModel;
    }
    
    protected function getParsURL($explodePar = "/busca/")
    {
        $server  = $this->getRequest()->getServer();
        $requestURI  = $server['REQUEST_URI'];
        $parts = explode($explodePar, $requestURI);
        
        $parsURI = array();
        if(isset($parts[1])) {
            $parsURI = explode("/", $parts[1]);
        }
        $pars = array();
        $numItens = count($parsURI);
        for($i = 0; $i < $numItens; ++$i)
        {   
            if(isset($parsURI[$i + 1])) {
                $pars[$parsURI[$i]] = $parsURI[$i + 1];
                ++$i;
            }
        }
        
        return $pars;  
    }
    
    /**
     * @return EntityManager     
     */
    function getEm() 
    {
        if(null == $this->em) {
              $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');  
        }
        
        return $this->em;
    }
    
    public function getClassName()
    {
        $className = explode("\\", get_class($this));  
        $classRoot = str_replace("controller", "", strtolower(end($className)) );
        
        return $classRoot;
    }
    
    public function stop()
    {
        exit();
    }
}