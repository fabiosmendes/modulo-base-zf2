<?php

namespace Base\File;

use Base\Filter\Inflector\Slug;

class Upload
{

    protected $slug;
    protected $em;

    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * 
     */
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    public static function resetFileName($string)
    {
        return $this->slug = new Slug($string, '-');
    }

    public static function pathFile($entity, $type)
    {
        $id = 0;
        $partsEntity = explode("\\", $entity);
        $entity = strtolower(end($partsEntity));
        $path = "./media/files/{$entity}/{$id}/";
        return $path;
    }

    public static function extensionFile($fullNameFile)
    {
        $exploed = explode('.', $fullNameFile);
        return "." . end($exploed);
    }

}
