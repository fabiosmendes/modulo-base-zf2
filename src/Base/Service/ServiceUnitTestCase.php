<?php

namespace Base\Service;

use PHPUnit_Framework_TestCase as TestCase;
//use Base\Fixture\TestDataFixture as TestCase;
use Mockery;

class ServiceUnitTestCase extends TestCase
{
    /**
     * ServiceManager
     */
    protected $sm;
    
    /**
     * EntityManager
     */
    protected $em;
    
    /**
     * Service
     */
    protected $service;
    
    /**
     * Service Name
     * @var string
     */
    protected $serviceName;
    
    /**
     * Entity instance
     * @var Base\Entity
     */
    protected $entity;
    
    public function getEmMock($entity = null)
    {   
        $entityManager = Mockery::mock('Doctrine\ORM\EntityManager');
        
        $entityManager->shouldReceive('insert')->andReturn($entity);
        $entityManager->shouldReceive('find')->andReturn($entityManager);
        $entityManager->shouldReceive('getRepository')->andReturn($entityManager);
        $entityManager->shouldReceive('persist')->andReturn($entityManager);
        $entityManager->shouldReceive('remove')->andReturn($entityManager);
        $entityManager->shouldReceive('flush')->andReturn($entityManager);
        
        return $entityManager;
    }
    
    public function getEm() 
    {
        $this->em = $this->sm->get('Doctrine\ORM\EntityManager');
        return $this->em;
    }
    
    public function tearDown()
    {
        unset($this->entity);
        
        parent::tearDown();
    }
}