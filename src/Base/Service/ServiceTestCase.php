<?php

namespace Base\Service;

use Base\Fixture\TestDataFixture;

class ServiceTestCase extends TestDataFixture
{
    /**
     * Service
     */
    protected $servico;
    
    /**
     * Service Name
     * @var string
     */
    protected $servicoNome;
    
    /**
     * Entity instance
     * @var Base\Entity
     */
    protected $entity;
    
    public function tearDown()
    {
        unset($this->entity);
        
        parent::tearDown();
    }
}