<?php

namespace Base\Service;

use Doctrine\ORM\EntityManager;
use Zend\Stdlib\Hydrator;

abstract class AbstractService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $entity;
    
    /**
     *
     * @var array 
     */
    protected $data;


    /**
     *
     * @var servive FileTransfer 
     */
    protected $fileTransfer;
    
    /**
     * 
     * @param string
     */
    protected $messages = array();
    
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function getList($pars = array(), $limit = 10)
    {
        $dql  = " SELECT p FROM ".$this->entity." p ";
        $dql .= " WHERE p.id > 0";
            
        if(isset($pars['title']) && $pars['title'] != '') {
            $dql .= " AND p.title LIKE '%".$pars['title']."%'";  
        }
        
        $dql .= ' ORDER BY p.id DESC';
        $query = $this->em->createQuery($dql);
        
        $lista = $query->getResult();
        return $lista;
    }
    
    public function insert(array $data)
    {
        $entity = new $this->entity($data);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    public function update(array $data)
    {
        $data['id'] = (int)$data['id'];
        
        if(!isset($data['id'])) {
            throw new \InvalidArgumentException('The key ID is required in array $data');
        }
        
        if(!is_integer($data['id'])) {
            throw new \InvalidArgumentException('The key ID must be numeric');
        }
        
        $entityRef = $this->em->getReference($this->entity, $data['id']);
       
        $hydrator = new Hydrator\ClassMethods();
        $hydrator->hydrate($data, $entityRef);

        $this->em->persist($entityRef);
        $this->em->flush();
        
        return $entityRef;
    }

    public function delete($id)
    {
        $id = (int)$id;
        if(!is_integer($id)) {
            throw new \InvalidArgumentException('The field ID must be numeric');
        }
        
        $entity = $this->em->getReference($this->entity, $id);
        if($entity) {
            $this->em->remove($entity);
            $this->em->flush();
            return $id;
        }
    }
    
    public function uploadFileManual($pathMedia, $postFile, $newName = '')
    {
        if(!is_array($postFile)) {
            return null;
        }
        
        $ext = pathinfo($postFile['name'], PATHINFO_EXTENSION);
        $extensions = array('png', 'gif', 'jpg', 'doc', 'docx', 'pdf', 'xls');
        
        if(!is_dir($pathMedia)) {
            mkdir($pathMedia, 0777);
        }
        
        if(in_array($ext, $extensions)) {
            if($newName == '') {
                $newName = \Base\Filter\Inflector\Slug::slugToLower($postFile['name']);
            }
            
            $newName .= '.'.$ext;
            $file = $pathMedia.$newName;
            if(move_uploaded_file($postFile['tmp_name'], $file)) {
                chmod($file, 0777);
                return $newName;
            } 
        }
        
        return null;
    }
    
    protected function getPathMedia()
    {
        $scriptFileName = $_SERVER['SCRIPT_FILENAME'];
        $path = str_replace("index.php", "", $scriptFileName);
        return $path."media";            
    }
    
    public function uploadFile($pathMedia, $file, $newName = '')
    {
        if(!isset($this->fileTransfer) || !($this->fileTransfer instanceof \Base\Http\FileTransfer)) {
            throw new \Exception("Service FileTransfer not set.");
        }
        
        sleep(1);
        // @todo check exists dir
        $this->fileTransfer->setPathFile($pathMedia);
        $this->fileTransfer->setFile($file);
        $this->fileTransfer->setNewFileName($newName);
        $this->fileTransfer->upload();
        return $this->fileTransfer->getNameFileUploaded();
    }
    
    public function removeDiretory($path)
    {
        @rmdir($path);
        return $path;   
    }
    
    public function removeFile($pathFile)
    {
        if(is_file($pathFile)) {
            unlink($pathFile);
        }
        return $pathFile;   
    }
    
    /**
     * 
     * @param type $entity
     * @return type
     */
    public function execute($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
    
    public function executeSqlReturnStatement($sql)
    {
        $stmt = $this->em->getConnection()->prepare($sql);
        try {
            $stmt->execute();
        }
        catch(\Exception $e) {
          echo $e->getMessage(); 
          exit();   
        }
        
        return $stmt;
    }
    
    public function executeSql($sql)
    {
        $stmt = $this->em->getConnection()->prepare($sql);
        try {
            $stmt->execute();
        }
        catch(\Exception $e) {
          echo $e->getMessage(); 
          exit();   
        }
        
        return $stmt->fetchAll();
    }

    public function getEntity()
    {
        return new $this->entity();
    }
    
    public function getEm()
    {
        return $this->em;
    }
    
    public function getRepository()
    {
        return $this->em->getRepository($this->entity);
    }
    
    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
    
    public function validateInsert()
    {
        $validate = true;
        return $validate;
    }
    
    public function validateUpdate()
    {
        $validate = true;
        return $validate;
    }
    
    public function addMessage($message)
    {
        $this->messages[] = $message;
    }
    
    public function addMessages($messages)
    {
        $this->messages = array_merge($this->messages, $messages);
    }
    
    public function getMessages()
    {
        return $this->messages;
    }
}