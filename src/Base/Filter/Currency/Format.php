<?php

namespace Base\Filter\Currency;

class Format
{

    /**
     * 
     * @param type $price
     * @return string
     */
    public static function _fixPriceToSave($price)
    {
        return str_replace(',', '.', preg_replace('/[^0-9(,)]/', '', $price));
    }

    /**
     * 
     * @param type $price
     * @param array $data
     * @return type
     */
    public static function convertFormatPrice($price, array $data = array())
    {
        $prefix = null;
        if (!empty($data['prefix'])) {
            $prefix = $data['prefix'];
            unset($data['prefix']);
        }
        $suffix = null;
        if (!empty($data['suffix'])) {
            $suffix = $data['suffix'];
            unset($data['suffix']);
        }

        $locale = 'pt_br';
        if (!empty($data['locale'])) {
            $locale = $data['locale'];
            unset($data['locale']);
        }


        if (mb_strtolower($locale) === 'pt_br') {
            $value = number_format($price, 2, ',', '.');
            return is_null($prefix) ? $value : "{$prefix} {$value}";
        }

        //ajustar esta linha abaixo para o locale que desejar e adicionar uma condicional para o locale
        $value = number_format($price, 2, ',', '.');
        $nValue = is_null($prefix) && is_null($suffix) ? $value : "{$prefix} {$value} {$suffix}";
        return trim($nValue);
    }

}
