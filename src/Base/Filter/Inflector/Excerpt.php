<?php

namespace Base\Filter\Inflector;

class Excerpt
{

    /**
     *
     * @param type $string
     * @param type $characters
     * @return string
     */
    public static function getExcerpt($string, $characters = 20)
    {
        $excerpt = explode(' ', $string, $characters);
        if (count($excerpt) >= $characters) {
            array_pop($excerpt);
            $excerpt = implode(" ", $excerpt) . '...';
        } else {
            $excerpt = implode(" ", $excerpt);
        }
        $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
        return $excerpt;
    }

}
