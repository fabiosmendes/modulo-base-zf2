<?php

namespace Base\Util;

use Zend\View\Helper\AbstractHelper;

class FormatarMoedaUtil extends AbstractHelper
{
    /**
     * @param   double $valor
     * @return  string
     * @desc:
     */
    static function formatoValorDouble($valor)
    {
        $valor = str_replace(",", ".", str_replace(".", "", $valor));
        $valor = str_replace("R$", "", $valor);
        $valor = trim(rtrim(ltrim($valor)));
        
        return $valor;
    }

    /**
     * @param   double $valor
     * @return  double
     */
    static function formatoValorReal($valor)
    {
        if(strstr($valor, ',')) {
            $pars = explode(',', $valor);

            if($pars[1] < 10)
                $pars[1] *= 10;

            return $pars[0] . ',' . $pars[1];
        }


        if(empty($valor))
            return '0,00';

        $vetor_valor = explode(".", $valor);
        $tam_vetor = strlen($vetor_valor[0]);
        $tam_valor = strlen($valor);


        if($tam_vetor > 3 && !empty($vetor_valor[1])) {
            $valor = substr($vetor_valor[0], 0, $tam_vetor - 3) . "." .
                    substr($vetor_valor[0], $tam_vetor - 3, $tam_vetor) . ",";

            if(strlen($vetor_valor[1]) < 2)
                $valor .= $vetor_valor[1] . "0";
            else
                $valor .= substr($vetor_valor[1], 0, 2);

            return $valor;
        }

        else if($tam_vetor <= 3 && !empty($vetor_valor[1])) {
            $valor = $vetor_valor[0] . ",";

            if(strlen($vetor_valor[1]) < 2)
                $valor .= $vetor_valor[1] . "0";
            else
                $valor .= substr($vetor_valor[1], 0, 2);

            return $valor;
        }

        else if($tam_valor > 3) {
            $valor = substr($valor, 0, $tam_valor - 3) . "." . substr($valor, $tam_valor - 3, $tam_valor) . ",00";
            return $valor;
        } else if($tam_valor <= 3) {
            $valor = $valor . ",00";
            return $valor;
        }
    }


}
