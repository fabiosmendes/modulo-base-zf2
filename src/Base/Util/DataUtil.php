<?php

namespace Base\Util;

class DataUtil
{
    /**
     * Recebe data BR e retorno objeto DateTime, com a data passada setada
     * 
     * @param string $dataBR
     * @return \DateTime|null
     */
    static function getDateTimeDataBR($dataBR)
    {
        if(!static::isDateBR($dataBR)) {
            return null;
        }
        
        $partsData = explode('/', $dataBR);
        $dataFormat = new \DateTime();
        $dataFormat->setDate($partsData[2], $partsData[1], $partsData[0]);

        return $dataFormat;
    }
    
    /**
     * 
     * @param   string
     * @return  string
     * @desc:   Trata uma data no formato americano e converte para o padrão em português.
     */
    static function getDataSYSHora($date = "", $parts = 3)
    {
        if(strlen($date) > 12) {
            $tmp = explode(" ", $date);
            $date = $tmp[0];
        }

        if(!empty($date)) {
            list($ano, $mes, $dia) = explode('-', $date);
            if($parts == 3)
                $date = $dia . "/" . $mes . "/" . $ano;
            else if($parts == 2)
                $date = $dia . "/" . $mes;
            else
                $date = $dia . "/" . $mes . "/" . $ano;

            return array($date, $tmp[1]);
        }
    }

    /**
     * 
     * @param   string
     * @return  string
     * @desc:   Trata uma data no formato americano e converte para o padrão em português.
     */
    static function DataSYSHora($date = "", $parts = 3)
    {
        if(strlen($date) > 12) {
            $tmp = explode(" ", $date);
            $date = $tmp[0];
        }

        if(!empty($date)) {
            list($ano, $mes, $dia) = explode('-', $date);
            if($parts == 3)
                $date = $dia . "/" . $mes . "/" . $ano;
            else if($parts == 2)
                $date = $dia . "/" . $mes;
            else
                $date = $dia . "/" . $mes . "/" . $ano;

            return $date . " " . $tmp[1];
        }
    }

    /**
     * 
     * @param   string
     * @return  string
     * @desc:   Trata uma data no formato americano e converte para o padrão em português.
     */
    static function DataSYS($date = "", $parts = 3, $datahora = false)
    {
        if(strlen($date) > 12) {
            $tmp = explode(" ", $date);
            $date = $tmp[0];
        }

        if(!empty($date)) {
            list($ano, $mes, $dia) = explode('-', $date);
            if($parts == 3)
                $date = $dia . "/" . $mes . "/" . $ano;
            else if($parts == 2)
                $date = $dia . "/" . $mes;
            else
                $date = $dia . "/" . $mes . "/" . $ano;

            if($datahora)
                return $date . ' ' . $tmp[1];

            return $date;
        }
    }

    /**
     * 
     * @param   void
     * @return  void
     * @desc:   Trata uma data no padrão português e converte para o formato americano.
     */
    static function DataDB($date = "")
    {
        if(!empty($date)) {
            list($dia, $mes, $ano) = explode('/', $date);
            $date = $ano . "-" . $mes . "-" . $dia;
            return $date;
        }
    }

    /**
     * @param  void
     * @return string data hora
     * @desc
     */
    static function getDataHora()
    {
        // '2011-09-01T22:00:00'
        return new \DateTime();
    }

    /**
     * @param  void
     * @return string hora
     * @desc
     */
    static function getHora()
    {
        return date('H:i:s');
    }

    /**
     * 
     * @param  void
     * @return string data
     * @desc
     */
    static function getData()
    {
        return date('Y-m-d');
    }
    
    static function isDateBR($data)
    {
        return (strstr($data, '/') != '');
    }

}
