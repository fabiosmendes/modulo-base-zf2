<?php

namespace Base\Util;

class UIDUtil
{
    // Private variable to hold current ID
    private $uid;
    private $randCalled;

    function __construct() {
        $this->uid = null;
    }

    // Return current ID
    function getUID()
    {
        // Generate ID if we have not done so already
        $this->generateUID();
        return substr($this->uid, 0, 6);
    }


    public function initRand()
    {
       $this->randCalled = false;

       if (!$this->randCalled)
       {
            srand((double) microtime() * 1000000);
            $this->randCalled = true;
       }
    }


    // Generate new ID and store in private variable
    public function generateUID()
    {
        $this->initRand();
        $this->uid = rand(1000, 9000) . date('his');
    }
}