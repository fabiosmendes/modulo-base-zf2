<?php

namespace Base\Util;

class Paginator
{
    public $total = 0;	
    public $limit = 0;
    public $pageCurrentNumber;
    public $currentPage;
    public $previousPage;
    public $nextPage;
    public $firstPage;
    public $lastPage;
    public $itensPerPage;
    public $numItensPagination;	
    public $pages;
    public $structure;

    public $isFirstPage;
    public $isLastPage;

	
    /**
     * Construtor
     * @param  void
     * @return void
     * @access public
     */
    public function __construct($total = 0, $numItens = 0)
    {
        if($total > 0)
            $this->setTotal($total);

        if($numItens > 0)
            $this->setItensPerPage($numItens);	
    }	
	
    /**
     * 	
     * @param  int $total
     * @return void
     * @access public
     * @date   29/05/2010 - 05:26:56
     */
    public function setTotal($total) 
    {
        $this->total = $total;		
    }
	
	
    /**
     * 	
     * @param  int $numItens
     * @return void
     * @access public
     * @date   30/05/2010 - 13:49:24
     */
    public function setItensPerPage($numItens) 
    {
        $this->itensPerPage = $numItens;
    }
	
	
    /**
     * 	
     * @param  int $page
     * @return void
     * @access public
     * @date   30/05/2010 - 13:53:11
     */
    public function setCurrentPage($page) 
    {
        $this->currentPage = $page;
    }
	
	
    /**
     * Número de itens na lista de paginação	
     * @param  int
     * @return void
     * @access public
     * @date   30/05/2010 - 14:03:55
     */
    public function setNumItensPagination($itensPagination) 
    {
        $this->numItensPagination = $itensPagination;
    }
	
	
    /**
     * Criar a estrutura de paginação	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 13:41:05
     */
    public function createStructure()
    {   
        $this->firstPage = 1;
        $this->lastPage  = ceil((int)$this->total / (int)$this->itensPerPage);
        // @todo - mudar indice de página sequencial
        
        /**
         * Calcular limite de exibição de páginas, alterando o inicio da lista de paginação, se necessário.
         * Controle para linha de paginação, se não exceder	
         */
        $initialLoop = 0;

        if($this->currentPage > 2)
            $pageCount   = $this->itensPerPage * ($this->currentPage - 1);
        else if($this->currentPage == 1)
            $pageCount   = 0;
        else	
            $pageCount   = $this->itensPerPage;

        $calcLoop    = $pageCount / $this->itensPerPage; 			
        $pageIndex   = ceil( ($calcLoop) /($this->numItensPagination - 1));

        $this->pageCurrentNumber = $calcLoop + 1;

        /**
         * Checar se é o primeira página
         */
        $this->isFirstPage = ($this->currentPage == 1);

        /**
         * Checar se é a última página
         */
        $this->isLastPage  = ($this->lastPage == $this->currentPage);

        /*
        //  
        if($pageIndex > 1)
        {		
                // O multiplicador serve guia para a multiplicação do ínicio da página	
                $mulplicadorIndex = $pageIndex - 1;
                $initialLoop 	  =  $this->numItensPagination * $mulplicadorIndex;
        }
        */	

        /**
         * Rever o inicio dos itens da paginação
         */
        $divItensPage = ceil($this->numItensPagination / 2);
        if( $this->pageCurrentNumber > $divItensPage)
        {
            $newBeginPagination = $this->pageCurrentNumber - $divItensPage;
            $newBeginPagination = ceil($newBeginPagination);			
            $initialLoop += $newBeginPagination;
        }


        $this->previousPage = 0;
        $this->nextPage	    = 0;

        /**
         * Looping para gerar a paginação
         */
        $this->pages = array();

        if($this->total == 0)
            return 0;

        for($i = $initialLoop; $i < ($this->numItensPagination + $initialLoop); ++$i)
        {
            $index = $i * $this->itensPerPage;	

            /**
             * Checar página atual
             */
            $checkCurrent = ($pageCount == $index);

            if($checkCurrent)
            {
                if($index > 0)
                    $this->previousPage = $this->pageCurrentNumber - 1;

                $this->nextPage	    = $this->pageCurrentNumber + 1;
            }

            /**
             * Vetor com os dados da página
             */
            $page = ($i + 1);
            $item = array('index'   => $index,
                          'page'    => $page,
                          'current' => $checkCurrent);

            /**
             * Seta páginas
             */
            $this->pages[] = $item;	

            if($this->lastPage == $page)
            {
                break;
            }	
        }	

    }
	
	
	
    /**
     * Verifica se o número de links da página é maior que o a última página	
     * @param  void
     * @return void
     * @access public
     * @date   01/06/2010 - 01:52:20
     */
    public function checkLimitPages() 
    {
        return ($this->numItensPagination > $this->lastPage);
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 21:07:05
     */
    public function isFirstPage() 
    {
        return $this->isFirstPage;
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 21:58:39
     */
    public function isLastPage() 
    {
        return $this->isLastPage;	
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 17:10:00
     */
    public function getFirstPage() 
    {
        return $this->firstPage;
    }
	
	
    /**
     * 	
     * @param  int $number
     * @return void
     * @access public
     * @date   30/05/2010 - 13:58:47
     */
    public function getPage($number) 
    {
        $page = $this->lastPage + ($number);
        return $page;
    }
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 17:50:16
     */
    public function getPreviousPage() 
    {
        return $this->previousPage;
    }
	
    /**
     * 	
     * @param  void
     * @return int $this->lastPage
     * @access public
     * @date   30/05/2010 - 13:58:47
     */
    public function getLastPage() 
    {
        return $this->lastPage;
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 17:50:33
     */
    public function getNextPage() 
    {
        return $this->nextPage;	
    }
    
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 17:27:06
     */
    public function getPageCurrentNumber() 
    {
        return $this->pageCurrentNumber;
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 13:44:14
     */
    public function getPages() 
    {
        return $this->pages;
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   01/06/2010 - 01:08:29
     */
    public function getItensPerPage() 
    {
        return $this->itensPerPage;
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   29/05/2010 - 05:26:00
     */
    public function getTotal() 
    {
        return $this->total;
    }	
	
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 18:04:21
     */
    public function getStructure() 
    {
        $this->structure['first'] = $this->getFirstPage();		
        $this->structure['next']  = $this->getNextPage();
        $this->structure['last']  = $this->getLastPage();
        $this->structure['previous'] = $this->getPreviousPage();
        $this->structure['current']  = $this->getPageCurrentNumber();
        $this->structure['pages']    = $this->getPages();
        $this->structure['total']    = $this->getTotal();

        return $this->structure;
    }
	
	
    /**
     * 	
     * @param  void
     * @return void
     * @access public
     * @date   30/05/2010 - 20:58:04
     */
    public function displayState() 
    {
        echo 'First Page:'. $this->getFirstPage();
        echo '<br/>';		
        echo 'Next Page:'. $this->getNextPage();
        echo '<br/>';
        echo 'Last Page:'. $this->getLastPage();
        echo '<br/>';
        echo 'Previous Page:'. $this->getPreviousPage();
        echo '<br/>';
        echo 'Current Page:'. $this->getPageCurrentNumber();
        echo '<br/>';
        echo 'Total Results: '.$this->getTotal();
        echo '<br/>';
        echo 'Page:'; 		
        echo '<pre>';
        var_dump($this->getPages());
    }
	
}