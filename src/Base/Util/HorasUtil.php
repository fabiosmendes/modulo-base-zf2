<?php

namespace Base\Util;

use Base\Util\StringUtil;

class HorasUtil {
    
    /**
     * 
     * @param int $horas
     * @param int $minutos
     * @return int
     */
    public function calcularTotalMinutos($horas, $minutos) {
        
        $horas = (int)$horas;
        $minutos = (int)$minutos;
        
        $totalMinutos = 0;
        $horasParaMinutos = 0;
        
        if($horas > 0) {
            $horasParaMinutos = $horas * 60;
            $totalMinutos += $horasParaMinutos;
        }
        
        if($minutos > 0) {
            $totalMinutos += $minutos;
        }
        
        return $totalMinutos;
    }
    
    
    /**
     * 
     * @param type $time
     * @param type $format
     * @return type
     */
    public function converterParaHorasMinutos($time, $format = '%s:%s') {
        settype($time, 'integer');
        if ($time < 1) {
            return;
        }
        $hours = floor($time/60);
        $minutes = $time%60;
        return sprintf($format, StringUtil::zerofill($hours, 2), StringUtil::zerofill($minutes, 2) );
    }
    
    
    /**
     * retorna o total de minutos de tempo passado no formato 00:00
     */
    public function getTotalMinutosTempo($tempo)
    {
        if(strstr($tempo, ":") != '') {
            $partsTempo = explode(":", $tempo);
            $horas      = $partsTempo[0]; 
            $minutos    = $partsTempo[1];
            
            return $this->calcularTotalMinutos($horas, $minutos);
        }
        
        return 0;
    }
}
