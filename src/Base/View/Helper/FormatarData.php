<?php

namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FormatarData extends AbstractHelper
{
    public function __invoke(\DateTime $dateTime)
    {
        return $dateTime->format('d/m/Y');
    }

}
