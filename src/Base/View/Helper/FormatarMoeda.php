<?php

namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

use Base\Util\FormatarMoedaUtil;

class FormatarMoeda extends AbstractHelper
{
    public function __invoke($numeral)
    {
        $formatoMoeda = new FormatarMoedaUtil();
        return $formatoMoeda->formatoValorReal($numeral);
    }

}
