<?php

namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Chamada no template:
 * echo $this->urlSite(); // retorna a url do site
 */
class UrlSite extends AbstractHelper
{
    public function __invoke()
    {
        $uriLocal  = 'http://'.$_SERVER['HTTP_HOST'];
        $uriLocal .= str_replace("index.php", "", $_SERVER['SCRIPT_NAME']);
        
        return $uriLocal;
    }

}
