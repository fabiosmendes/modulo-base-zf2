<?php

namespace Base\View;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

/**
 * Solução baseada encontrada no link:
 * http://stackoverflow.com/questions/10298859/zend-framework-2-unable-to-render-template-resolver-could-not-resolve-to-a-fil
 */
class LoadTemplate 
{    
    /**
     * Seta váriaveis e retorna o template para um 
     * template html.
     * 
     * @param type $tpl
     * @param type $moduleName
     * @param array $pars
     * @return string
     */
    public function get($tpl, $moduleName, array $pars = array()) 
    {
        $renderer = new PhpRenderer();
        
        $partDir = explode('module', __DIR__);
        $pathTpl = $partDir[0].'module/'.$moduleName.'/view/'.$tpl.'.phtml'; 
        
        $map = new TemplateMapResolver(array(
            'html' => $pathTpl,
        ));
        
        $resolver = new TemplateMapResolver($map);
        $renderer->setResolver($resolver);
        //$renderer->setHelperPluginManager($pathTpl);
        $model = new ViewModel($pars);
        $model->setTemplate('html');
        
        return $renderer->render($model);
    }
}
