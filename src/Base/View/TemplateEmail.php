<?php

namespace Base\View;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplatePathStack;
use Zend\View\Resolver\TemplateMapResolver;

/**
 * Solução baseada no solução entrada no link:
 * http://stackoverflow.com/questions/10298859/zend-framework-2-unable-to-render-template-resolver-could-not-resolve-to-a-fil
 */
class TemplateEmail {
    
    /**
     * Seta váriaveis e retorna o template para um 
     * template de e-mail.
     * 
     * @param type $tplEmail
     * @param type $moduleName
     * @param array $pars
     * @return string
     */
    public function gerar($tplEmail, $moduleName, array $pars = array()) {
        $renderer = new PhpRenderer();
        
        $partDir = explode('module', __DIR__);
        $pathEmail = $partDir[0].'module/'.$moduleName.'/view/email/'.$tplEmail.'.phtml'; 
        
        $map = new TemplateMapResolver(array(
            'email' => $pathEmail,
        ));
        
        $resolver = new TemplateMapResolver($map);
        $renderer->setResolver($resolver);
       
        $model = new ViewModel($pars);
        $model->setTemplate('email');
        
        return $renderer->render($model);
    }
}
