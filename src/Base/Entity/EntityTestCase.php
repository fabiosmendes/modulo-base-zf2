<?php

namespace Base\Entity;

use Base\Fixture\TestDataFixture;

class EntityTestCase extends TestDataFixture
{
    /**
     * Entity
     */
    protected $entity;
    
    /**
     * Return format attribut for use in method name - 
     * Ex: input  - user_name
     *     output - UserName
     * 
     * @param string $attribute
     * @return string $attribute
     */
    public function formatAttribute($attribute)
    {
        if(strstr($attribute, "_") != '') {
            $partsAttribute = explode('_', $attribute);
            $attribute = '';
            foreach($partsAttribute as $part) {
                $attribute .= ucfirst($part);
            }
            
        } else {
           $attribute = ucfirst($attribute); 
        }
        
        return $attribute;
    }

    public function tearDown()
    {
        unset($this->entity);

        parent::tearDown();
    }
}
