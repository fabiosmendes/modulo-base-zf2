<?php

namespace Base\Form;

use PHPUnit_Framework_TestCase as TestCase;

class FormTestCase extends TestCase
{
    /**
     * Class Name
     * @var string
     */
    protected $class;

    /**
     * Form instance
     * @var Zend\Form\Form
     */
    protected $form;
    
    
    public function tearDown()
    {
        unset($this->class, $this->form);
        parent::tearDown();
    }
}