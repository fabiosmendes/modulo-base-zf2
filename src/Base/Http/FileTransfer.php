<?php

namespace Base\Http;

use Base\Util\StringUtil;

class FileTransfer
{
    private $file;
    private $pathFile;
    private $newFileName = null;
    private $fileNameUploaded;
    private $messages;
    protected $adapter;
    protected $validators;
    protected $filters;

    /**
     * @var array Error message templates
     */
    public $configValidatorSize = array('min' => 1, 'max' => 500000);
    public $configValidatorExt = array('extension' => array('jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls'));
    public $configFilterRename = array(); // "target" => './public/files/clientes/img', "randomize" => true,

    public function __construct($adapter, $validators = array(), $filters = array())
    {
        $this->adapter    = $adapter;
        $this->validators = $validators;
        $this->filters    = $filters;
    }

    public function setValidators()
    {
        if(count($this->validators) > 0) {
            foreach($this->validators as $validator) {
                if($validator instanceof \Zend\Validator\File\Size) {
                    $validator->setOptions($this->configValidatorSize);
                } else if($validator instanceof \Zend\Validator\File\Extension) {
                    $validator->setOptions($this->configValidatorExt);
                }
            }
        }
    }

    protected function setConfigFilterRename($config = array())
    {
        if(is_array($config)) {
            $this->configFilterRename = $config;
        }
    }

    public function setFilters()
    {
        if(count($this->filters) > 0) {
            foreach($this->filters as $validator) {
                if($validator instanceof \Zend\Filter\File\Rename &&
                        count($this->configFilterRename) > 0) {
                    $validator->setOptions($this->configFilterRename, $this->file['name']);
                }
            }
        }
    }
    
    public function setConfig($config = array())
    {
        // @todo - implementar envio de configuração via array
    }

    public function setNewFileName($newFileName)
    {
        $this->newFileName = $newFileName;
        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    public function getPathFile()
    {
        return $this->pathFile;
    }

    public function setPathFile($pathFile)
    {
        if(!is_dir($pathFile)) {
            mkdir($pathFile, 0777);
        }
            
        $this->pathFile = $pathFile;
        return $this;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Efetuar upload do arquivo setado, utilizando os recursos dos validators
     * e dos filtros 
     *
     * @param  string $file = null
     * @return bool
     */
    public function upload($file = null)
    {
        if($this->pathFile == null) {
            return false;
        }    
        
        if($file != null) {
            $this->file = $file;
        }
        
        if($this->file != null && is_array($this->file)) {
            $this->setValidators();
            
            $ext = pathinfo($this->file['name'], PATHINFO_EXTENSION);
            
            if($this->newFileName !== null) {
                // @todo - repensar serviço com adição do método addFilter e addValidators
                $this->adapter->addFilter('Rename', 
                                          array('target'    => $this->pathFile . $this->newFileName . '.' . $ext,
                                                'overwrite' => false), 
                                          $this->file['name']);
            } else {
                $this->adapter->setDestination($this->pathFile);
            }
            
            if($this->adapter->receive($this->file['name'])) {
                $fileName = $this->adapter->getFileName();
                
                if(!strstr(php_uname('s'), 'Windows') ) {
                    if(is_array($fileName)) {
                    foreach($fileName as $file) {
                            chmod($file, 0777);
                        }
                    } else if(is_string($fileName)) {
                        chmod($fileName, 0777);
                    }
                }
                
                $separadorPath = '/';
                if(strstr($fileName, '\\')) {
                    $separadorPath = '\\';
                }
                $partsFile = explode($separadorPath, $fileName);
                $this->fileNameUploaded = end($partsFile);
                unset($this->file);
                return $fileName;
            }
        }

        return false;
    }

    
    public function getNameFileUploaded()
    {
        return $this->fileNameUploaded;
    }
}
