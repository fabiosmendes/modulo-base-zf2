<?php

namespace Base\Http;

use Zend\Http\PhpEnvironment\Request as HttpRequest;
use Base\Util\StringUtil;

class UrlMeta 
{
    /**
     *
     * @var HttpRequest 
     */
    private $request;
    
    public function __construct(HttpRequest $request)
    {
        $this->request = $request;
    }
    
    public function getParsURL($explodePar = "/busca/")
    {
        $server      = $this->request->getServer();
        $requestURI  = $server['REQUEST_URI'];
        $parts = explode($explodePar, $requestURI);
        
        $parsURI = array();
        if(isset($parts[1])) {
            $parsURI = explode("/", $parts[1]);
        }
        
        $pars = array();
        $numItens = count($parsURI);
        for($i = 0; $i < $numItens; ++$i) {   
            if(isset($parsURI[$i + 1])) {
                $pars[$parsURI[$i]] = $parsURI[$i + 1];
                ++$i;
            }
        }
        
        return $pars;  
    }
    
    
    /**
     * Pegar um item na url como valor, baseado no item anterior.
     * Ex: 
     * url -> /chave/roda
     * $this->getItemNaUrl('chave') retornará roda
     * 
     * @param  string  $itemAnterior
     * @param  string  $indice
     * @return string  $item
     * 
     */
    public function getItemNaUrl($itemAnterior, $indice = 1)
    {	
        $uri = $_SERVER['REQUEST_URI']; 
        
        if(strstr($uri, "?") != '') {
            $partsSlug   = explode("?", $uri);
            $uri = $partsSlug[0];
        }

        $itens    = explode('/', $uri);
        $numItens = count($itens);

        $item  = "";

        for($i = 0; $i < $numItens; $i++) {
            if($itens[$i] == $itemAnterior) {
                if(isset($itens[$i + $indice])) {
                    $item = $itens[$i + $indice];
                    break;
                }
            } 
        }

        return $item;
    }
    
    
    /**
     * @param  $string $url
     * @return string  $item
     * @desc   pegar o último item da url, útil quando para ser usado em buscas
     */
    public function getLastItemUrl($url)
    {
        $item = '';
        
        if(!strstr($url, '/')) {
           return $item; 
        }
            
        
        $parts = explode('/', $url);
        $item = $parts[count($parts) - 1];
        
        if($item == '') {
            $item = $parts[count($parts) - 2];
        }
        
        return $item;
    }

    /**
     * Pegar um item na url como valor, baseado no item posterior.
     * Ex: 
     * url -> /chave/roda
     * $this->getItemAnteriorNaUrl('roda') retornará chave
     * 
     * @param  string  $itemPosterior
     * @return string  $item
     * 
     */
    public function getItemAnteriorNaUrl($itemPosterior, $indice = 1)
    {	
        $uri   = $_SERVER['REQUEST_URI']; 
        $itens = explode('/', $uri);
        $numItens = count($itens);
        $item  = "";
        
        for($i = 0; $i < $numItens; $i++) {
            if($itens[$i] == $itemPosterior) {
                if(isset($itens[$i-$indice])) {
                    $item = $itens[$i-$indice];
                    break;
                }
            } 
        }
        
        return $item;
    }
    
    public function getPath()
    {        
        return $this->request->getUri()->normalize()->getPath();   
    }
    /*
     * if(isset($_SERVER['HTTP_REFERER'])) {
                $linkPagina = $_SERVER['HTTP_REFERER'];
                if(strstr($linkPagina, "pagina")) { // correção para montar a url de busca
                    $parsLink = explode("/pagina", $linkPagina);
                    $linkPagina = $parsLink[0];
                }    
            }
     */
    
    public function getHttpReferer()
    {
        if(isset($_SERVER['HTTP_REFERER'])) {
            $linkPagina = $_SERVER['HTTP_REFERER'];
            if(strstr($linkPagina, "pagina")) { // correção para montar a url de busca
                $parsLink = explode("/pagina", $linkPagina);
                $linkPagina = $parsLink[0];
            }    
        }
    }
}