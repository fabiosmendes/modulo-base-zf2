<?php

namespace Base\Mail;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Message;
use Zend\View\Model\ViewModel;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Mime;

class Mail
{
    protected $transport;
    protected $view;
    protected $body;
    protected $message;
    protected $subject;
    protected $to;
    protected $data;
    protected $page;

    /**
     * @var array
     */
    private $attachments = array();

    public function __construct(SmtpTransport $transport, $view, $page = '')
    {
        $this->transport = $transport;
        $this->view = $view;
        $this->page = $page;

        $this->message = new Message();
    }
    
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }
    
    public function setSubject($subject)
    {
        $this->subject = utf8_decode($subject);
        return $this;
    }

    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function logMail($html)
    {
        if(strstr($_SERVER['HTTP_HOST'],'localhost') != '') {
            $pathLog = str_replace("index.php", "", $_SERVER['SCRIPT_FILENAME']) . "../tmp/";
            $arquivo = $this->to . date('H_i_s') . '.html';
            $pathFile = $pathLog . $arquivo;

            $f = fopen($pathFile, "w+");
            fwrite($f, $html);
            fclose($f);
        }
    }

    public function renderView($page, array $data)
    {
        $model = new ViewModel();
        $model->setTemplate("email/{$page}.phtml");
        $model->setOption('has_parent', true);
        $model->setVariables($data);
        $html = $this->view->render($model);
        $this->logMail($html);
        return utf8_decode($html);
    }

    public function prepare()
    {
        $html = new MimePart($this->renderView($this->page, $this->data));
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->setParts(array($html));
        $this->body = $body;

        $config = $this->transport->getOptions()->toArray();

        $this->message = new Message();
        $this->message->addFrom($config['connection_config']['from'])
                ->addTo($this->to)
                ->setSubject($this->subject)
                ->setBody($this->body);
        
        if(count($this->attachments) > 0) {
            $mimeMessage = $this->message->getBody();
            if(!$mimeMessage instanceof MimeMessage) {
                $this->setBody(new MimePart($mimeMessage));
                $mimeMessage = $this->message->getBody();
            }

            $bodyContent = $mimeMessage->generateMessage();
            $bodyPart = new MimePart($bodyContent);
            $bodyPart->type = Mime::TYPE_HTML; // TODO
            $attachmentParts = array();
            $info = new \finfo(FILEINFO_MIME_TYPE);
            foreach($this->attachments as $attachment) {
                if(!is_file($attachment))
                    continue; // If checked file is not valid, continue to the next

                $part = new MimePart(fopen($attachment, 'r'));
                $part->filename = basename($attachment);
                $part->type = $info->file($attachment);
                $part->encoding = Mime::ENCODING_BASE64;
                $part->disposition = Mime::DISPOSITION_ATTACHMENT;
                $attachmentParts[] = $part;
            } 
            
            array_unshift($attachmentParts, $bodyPart);
            $body = new MimeMessage();
            $body->setParts($attachmentParts);
            $this->message->setBody($body);
        }
        
        return $this;
    }
    
    public function send()
    {
        if(!strstr($_SERVER['HTTP_HOST'],'localhost')) {
            $this->transport->send($this->message);
        }
    }

    /**
     * 
     * @return Returns this MailService for chaining purposes
     * @see \AcMailer\Service\MailServiceInterface::addAttachment()
     */
    public function addAttachment($path)
    {
        $this->attachments[] = $path;
        return $this;
    }

    /**
     * 
     * @return Returns this MailService for chaining purposes
     * @see \AcMailer\Service\MailServiceInterface::addAttachments()
     */
    public function addAttachments(array $paths)
    {
        $this->attachments = array_merge($this->attachments, $paths);
        return $this;
    }

    /**
     * 
     * @return Returns this MailService for chaining purposes
     * @see \AcMailer\Service\MailServiceInterface::setAttachments()
     */
    public function setAttachments(array $paths)
    {
        $this->attachments = $paths;
        return $this;
    }

}
