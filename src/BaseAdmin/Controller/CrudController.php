<?php

namespace BaseAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;

use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

use Base\Util\StringUtil;
use Base\Util\Paginator as Paginacao;

class CrudController extends AbstractActionController 
{
    /**
     * @var EntityManager 
     */
    protected $em;
    protected $authService;
    protected $service;
    protected $entity;
    protected $form;
    protected $route;
    protected $controller;
    
    public function __construct() 
    {
        $this->layout()->controllerName = array('control' => $this->controller);
    }
    
    public function getAuthService()
    {
        return $this->authService;
    }

    public function getIdentity($namespace = null)
    {
        $sessionStorage = new SessionStorage($namespace);
        $this->authService = new AuthenticationService();
        $this->authService->setStorage($sessionStorage);

        if($this->getAuthService()->hasIdentity()) {
            return $this->getAuthService()->getIdentity();
        }
    }
    
    public function indexAction() 
    {   
        $service  = $this->getServiceLocator()->get($this->service);
        $searchPars = $this->params()->fromQuery();
        $list     = $service->getList($searchPars);
        
        $page = $this->params()->fromRoute('pager');    
        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);
        
        $flashMessenger = $this->getServiceLocator()->get('ServiceFlashMessenger');
        $message = '';
        if ($flashMessenger->hasMessages()) {
            $messages = $flashMessenger->getMessages(); // segmentar mensagens (erro, por exemplo e success)
            $message = $messages[0];   
        }
        
        return $this->renderView(array('data' => $paginator, 
                                       'page' => $page, 
                                       'message'   => $message, 
                                       'queryPars' => json_encode($searchPars)));
    }
    
    public function viewAction() 
    {
        $repository  = $this->getEm()->getRepository($this->entity);
        $id          = $this->params()->fromRoute('id', 0);
        $entity      = $repository->find($id);
        
        return $this->renderView(array('entity' => $entity));
    }
    
    public function newAction() 
    {
        $form = new $this->form();
        
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $service->insert($postData);
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    
    public function editAction() 
    {
        $form = new $this->form();
        
        $request = $this->getRequest();
        
        if($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $form->setData($postData);
            if($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                $service->update($postData);
                
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
        }
        else {
            $repository  = $this->getEm()->getRepository($this->entity);
            $id          = $this->params()->fromRoute('id', 0);
            $entity      = $repository->find($id);
            
            if(null == $entity) {
                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
            }
            else { 
                $form->setData($entity->toArray());
            }  
        }
        
        return $this->renderView(array('form' => $form));
    }
    
    public function deleteAction() 
    {
        $service = $this->getServiceLocator()->get($this->service);
        if($service->delete($this->params()->fromRoute('id', 0))) {
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
        }
            
    }
    
    
    public function renderView(array $pars) 
    {    
        // Seta nome do controller para marcação do Menu
        $this->layout()->controllerName = $this->controller;
        
        $view = new ViewModel();
        $view->setVariables($pars);
        
        return $view;
    }
    
    
    /**
     * @return EntityManager     
     */
    protected function getEm() 
    {
        if(null == $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');  
        }
        
        return $this->em;
    }
    
    public function setMessagesForm($form, $messages)
    {
        foreach($messages as $field => $fieldMessages) {
            $form->get($field)->setMessages($fieldMessages);
        }
    }
    
    public function stop()
    {
        exit();
    }
}