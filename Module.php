<?php
namespace Base;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Zend\Authentication\AuthenticationService;

use Base\Http\FileTransfer;
use Base\View\Helper\FormatarMoeda;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function onBootstrap($e) 
    {
        
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__."Admin" => __DIR__ . '/src/' . __NAMESPACE__."Admin",
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig() 
    {    
        return array(
          'factories' => array(
              'ServiceFlashMessenger' => function($service) {
                return $service->get('ControllerPluginManager')
                               ->get('flashMessenger');
              },
              'Base\Mail\Transport' => function($sm) {
                $config = $sm->get('Config');
                
                $transport = new SmtpTransport();
                $options = new SmtpOptions($config['mail']);
                $transport->setOptions($options);
                
                return $transport;
              },
              'Base\Http\FileTransfer' => function() {
                  $adapter    = new \Zend\File\Transfer\Adapter\Http(); 
                  
                  /**
                   * Validators
                   */
                  $size       = new \Zend\Validator\File\Size(); // minimum bytes filesize, max too..
                  $extension  = new \Zend\Validator\File\Extension(array());
                  $validators = array($size, $extension);
                  
                  /**
                   * Filters
                   */
                  $renameFilter     = new \Zend\Filter\File\Rename(array());
                  $filters    = array($renameFilter);
                  
                  $fileTransfer = new FileTransfer($adapter, $validators, $filters);
                  
                  return $fileTransfer;
              },
              'Base\Mail\Mail' => function($sm) {
                  return new \Base\Mail\Mail($sm->get('Base\Mail\Transport'), 
                                             $sm->get('ViewRenderer'));
              },              
              'Zend\Authentication\AuthenticationService' => function() {
                  return  new AuthenticationService();
              },
              'SessionManager' => function() {
                  $sessionContainer = new \Zend\Session\Container();
                  return  $sessionContainer;
              },        
          )  
       );
    }
    
    public function getViewHelperConfig() {
        
        return array(
            'factories' => array(
                'formatMoeda'  => function($sm) {
                    // @todo - preparar para idioma no constructor ou opção
                    return new FormatarMoeda();
                },
            ),     
        );
    }
}
